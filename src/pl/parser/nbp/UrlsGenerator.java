package pl.parser.nbp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dominik on 2016-05-23.
 */
public class UrlsGenerator {
    private LocalDate start,end;

    public UrlsGenerator(LocalDate start, LocalDate end) {
        this.start = start;
        this.end = end;
    }

    public ArrayList<URL> getURLsToXMLs() throws IOException {
        ArrayList<URL> tmp = new ArrayList<>();
        for (String s : getDirsToSearch()) {
            tmp.add(new URL("http://www.nbp.pl/kursy/xml/" + s));
        }

        ArrayList<URL> xmlFiles = new ArrayList<>();
        for (URL dir : tmp) {
            for(String s : getFilesToParse(dir)){
                xmlFiles.add(new URL("http://www.nbp.pl/kursy/xml/" + s + ".xml"));
            }
        }
        return  xmlFiles;
    }

    private ArrayList<String> getFilesToParse(URL listOfFiles) throws IOException {
        ArrayList<String> list = new ArrayList<>();

        BufferedReader in = new BufferedReader(new InputStreamReader(listOfFiles.openStream()));

        Matcher m = Pattern.compile("^(c)(\\d{3})(z)(\\d{6})").matcher("");
        String str;
        while ((str = in.readLine()) != null) {
            m.reset(str);
            if (!m.find())
                continue;
            list.add(str);
        }
        in.close();
        return list;
    }
    private ArrayList<String> getDirsToSearch() {
        ArrayList<String> list = new ArrayList<>();
        for (Integer i : getYears()) {
            if (i == LocalDate.now().getYear())
                list.add("dir.txt");
            else
                list.add("dir" + i + ".txt");
        }
        return list;
    }
    private ArrayList<Integer> getYears() {
        ArrayList<Integer> list = new ArrayList<>();
        for (LocalDate i = LocalDate.from(start); i.isBefore(end); i = i.plusYears(1)) {
            list.add(i.getYear());
        }
        return list;
    }
}

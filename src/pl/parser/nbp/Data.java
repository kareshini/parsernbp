package pl.parser.nbp;

import java.time.LocalDate;

/**
 * Created by Dominik on 2016-05-22.
 */
public class Data implements Cloneable {
    private double kurs_kupna,
            kurs_sprzedazy;
    private String kod_waluty;
    private LocalDate data_publikacji;

    public Data(String kod_waluty) {
        this.kod_waluty = kod_waluty;
    }

    public LocalDate getData_publikacji() {
        return data_publikacji;
    }

    public void setData_publikacji(String data_publikacji) {
        this.data_publikacji = LocalDate.parse(data_publikacji);//DateTimeFormatter.ISO_LOCAL_DATE year-month-day
    }

    public double getKurs_sprzedazy() {
        return kurs_sprzedazy;
    }

    public void setKurs_sprzedazy(double kurs_sprzedazy) {
        this.kurs_sprzedazy = kurs_sprzedazy;
    }

    public double getKurs_kupna() {
        return kurs_kupna;
    }

    public void setKurs_kupna(double kurs_kupna) {
        this.kurs_kupna = kurs_kupna;
    }

    public String getKod_waluty() {
        return kod_waluty;
    }
    public Data clone(){
        Data newTMP = new Data(this.kod_waluty);
        newTMP.setData_publikacji(this.data_publikacji.toString());
        newTMP.setKurs_kupna(this.kurs_kupna);
        newTMP.setKurs_sprzedazy(this.kurs_sprzedazy);
        return newTMP;
    }
}

package pl.parser.nbp;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class MainClass {

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        List<String> listOfCurrencies = Arrays.asList("USD", "EUR", "CHF", "GBP");
        if (!listOfCurrencies.contains(args[0])) {
            throw new RuntimeException("Wrong currency code");
        }

        LocalDate start = LocalDate.parse(args[1]); //DateTimeFormatter.ISO_LOCAL_DATE year-month-day
        LocalDate end = LocalDate.parse(args[2]);

        ConcurrentLinkedQueue<Data> queue = new ConcurrentLinkedQueue<>();
        UrlsGenerator generator = new UrlsGenerator(start, end);
        ArrayList<URL> xmls = generator.getURLsToXMLs();


        ArrayList<Thread> producers = new ArrayList<>();

        int numberOfProducers = 3; //easy way to find optimal numbers of producer threads
        int chunk = xmls.size() / numberOfProducers;
        for (int i = 0; i < numberOfProducers; i++) {
            if (i == numberOfProducers - 1)
                producers.add(new Thread(new Producer(xmls.subList(chunk * i, xmls.size() - 1), queue, args[0])));
            else
                producers.add(new Thread(new Producer(xmls.subList(chunk * i, chunk * (i + 1)), queue, args[0])));
        }

        FutureTask<Consumer.Pair> futureTask = new FutureTask<>(new Consumer(start, end, queue));
        Thread consumer = new Thread(futureTask);

        producers.forEach(e -> e.start());
        consumer.start();
        for (Thread t : producers) {
            t.join();
        }
        consumer.interrupt();

        Consumer.Pair average = futureTask.get();

        double deviation = 0;
        int counter = 0;
        for (Data data : queue) {
            deviation += Math.pow((data.getKurs_sprzedazy() - average.snd), 2);
            counter++;
        }
        deviation /= counter;
        deviation = Math.sqrt(deviation);

        System.out.format("%.4f%n", average.fst);
        System.out.format("%.4f%n", deviation);
    }


}

package pl.parser.nbp;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Dominik on 2016-05-22.
 */
public class SaxHandler extends DefaultHandler {

    private Data tmp;
    private boolean wlasciwa_waluta;

    private Stack<String> elementStack = new Stack<>();

    public ConcurrentLinkedQueue<Data> dataQueue;

    public SaxHandler(String kod_waluty , ConcurrentLinkedQueue queue) {
        tmp = new Data(kod_waluty);
        this.dataQueue = queue;
    }



    public void startElement(String uri, String localName,
                             String qName, Attributes attributes)
            throws SAXException {

        this.elementStack.push(qName);
    }

    public void characters(char ch[], int start, int length)
            throws SAXException {

        String value = new String(ch, start, length).trim();

        switch (currentElement()) {
            case "data_publikacji":
                tmp.setData_publikacji(value);
                break;
            case "kod_waluty":
                if (tmp.getKod_waluty().equals(value))
                    wlasciwa_waluta = true;
                else
                    wlasciwa_waluta = false;
                break;
            case "kurs_kupna":
                if (wlasciwa_waluta)
                    tmp.setKurs_kupna(Double.parseDouble(value.replace(',','.')));
                break;
            case "kurs_sprzedazy":
                if (wlasciwa_waluta)
                    tmp.setKurs_sprzedazy(Double.parseDouble(value.replace(',','.')));
        }
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        this.elementStack.pop();
    }

    public void endDocument() throws SAXException {
        dataQueue.add(tmp.clone());
    }

    private String currentElement() {
        return this.elementStack.peek();
    }

//    @Override
//    public String toString() {
//        return tmp.getData_publikacji() + " : " + tmp.getKurs_kupna() + " : " + tmp.getKurs_sprzedazy();
//    }
}

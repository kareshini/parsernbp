package pl.parser.nbp;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Dominik on 2016-05-22.
 */
public class Producer implements Runnable {

    private List<URL> listOfXMLs;
    private ConcurrentLinkedQueue<Data> dataQueue;
    private String currency ;

    public Producer(List<URL> listOfXmls , ConcurrentLinkedQueue<Data> dataQueue , String currency) {
        this.dataQueue = dataQueue;
        this.currency = currency;
        this.listOfXMLs = listOfXmls;
    }

    @Override
    public void run() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = null;
        try {
            saxParser = factory.newSAXParser();
        } catch (ParserConfigurationException |SAXException e) {
            throw new Error("Unable to create XML parser",e);
        }


        for(URL url : listOfXMLs) {
            try(InputStream xmlInput = url.openStream()){
                DefaultHandler handler   = new SaxHandler(currency,dataQueue);
                saxParser.parse(xmlInput, handler);

            } catch (IOException e) { //Error because we need all datas to have correct result.
                throw new Error("Unable to open stream from url. Url is from List<URL> (constructor)",e);
            } catch (SAXException e) {//Error because we need all datas to have correct result.
                throw new Error("Some errors with parsing using SaxHandler",e);
            }
        }
    }


}

package pl.parser.nbp;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Dominik on 2016-05-22.
 */
public class Consumer implements Callable<Consumer.Pair> {
    public class Pair{
        public double fst;
        public double snd;
    }
    private LocalDate start, end;
    private int  counter = 0;
    private Pair average = new Pair();
    private boolean producerFinished = false;

    private ConcurrentLinkedQueue<Data> dataQueue;

    public Consumer(LocalDate start, LocalDate end, ConcurrentLinkedQueue<Data> dataQueue) {
        this.start = start;
        this.end = end;
        this.dataQueue = dataQueue;
    }

    @Override
    public Pair call() {

        Data data;
        ArrayList<Data> tmp = new ArrayList<>();

        while (true) {
            data = dataQueue.poll();
            if(Thread.currentThread().isInterrupted()) {
                producerFinished = true;
            }

            if(data == null) {
                if(producerFinished)
                    break;
                else
                    continue;
            }

            if (data.getData_publikacji().isBefore(start) || data.getData_publikacji().isAfter(end))
                continue;
            average.fst += data.getKurs_kupna();
            average.snd += data.getKurs_sprzedazy();
            counter++;

            tmp.add(data);
        }

        tmp.forEach(e -> dataQueue.add(e));

        average.fst /= counter;
        average.snd /= counter;

        return average;
    }



}
